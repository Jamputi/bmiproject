//
//  Person.swift
//  BMI
//
//  Created by Jalmari Espo on 27.3.2020.
//  Copyright © 2020 Jalmari Espo. All rights reserved.
//

import UIKit

class Person {
    let name: String
    let bmi: Double
    private (set)var weight = 80
    private (set)var height = 185
    private (set)var age = 22
    let minWeight = 45
    let minHeight = 145
    let maxAge = 100
    var professions = [String]()

    init( name: String,  weight: Int,  height: Int) {
        self.name = name
        self.weight = weight
        self.height = height
        self.bmi = ((Double(weight) / (pow(((Double(height) / 100)), 2))) * 10).rounded() / 10
    }

    func setHeight( height: Int) {
        if (height >= minHeight) {
            self.height = height
        } else {
            self.height = minHeight
        }
    }

    func setWeight( weight: Int) {
        if (weight >= minWeight) {
            self.weight = weight
        } else {
            self.weight = minWeight
        }
    }

    func setAge( newAge: Int) {
        if (newAge < age) {
            print("Cant get younger")
        } else if (newAge > maxAge) {
            print("Too old")
        } else {
            self.age = newAge
        }
    }

    func setProfession(_ prof: String) {
        if (professions.count < 5) {
            professions.append(prof)
        }
    }

}
