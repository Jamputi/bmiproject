//
//  ViewController.swift
//  BMI
//
//  Created by Jalmari Espo on 27.3.2020.
//  Copyright © 2020 Jalmari Espo. All rights reserved.
// halooo

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var weightHeight: UIPickerView!
    @IBOutlet weak var calculateBMI: UIButton!
    @IBOutlet weak var result: UILabel!
    
       let weightArray = [Int](30...150)
       let heightArray = [Int](120...250)
       var name = ""
       var weight = 80
       var height = 170
    
    // textField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // display alert if nameinput is empty
        if textField.text == "" {
            let alert = UIAlertController(title: "Name is missing", message: "Please enter your name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        textField.resignFirstResponder()
        calculateBMI.isEnabled = true
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        name = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacter = CharacterSet.letters
        let allowedCharacter1 = CharacterSet.whitespaces
        let characterSet = CharacterSet(charactersIn: string)
        
        return allowedCharacter.isSuperset(of: characterSet) || allowedCharacter1.isSuperset(of: characterSet)
    }
    
    // UIPickerDelegate
       // set arrays to different components
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           if component == 0 {
               return "\(weightArray[row]) kg"
            }
           return "\(heightArray[row]) cm"
       }
       // User picks the rows
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           if component == 0 {
               weight = weightArray[row]
           } else {
               height = heightArray[row]
           }
       }
       
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return weightArray.count
        }
        return heightArray.count
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.delegate = self
        weightHeight.delegate = self as UIPickerViewDelegate
        weightHeight.dataSource = self as UIPickerViewDataSource
        // Do any additional setup after loading the view.
    }
    
    //Actions
    @IBAction func calculateBMI(_ sender: UIButton) {
        let person = Person(name: name, weight: Int(Double(weight)), height: Int(Double(height)))
        result.text = "\(person.name)'s bmi: \(person.bmi)"
        nameField.text = ""
        name = ""
        calculateBMI.isEnabled = false
    }

}

